<?php
require_once("utils/databaseManager.php");
$title = "Pokedex";

// $passHash = password_hash("admin", PASSWORD_DEFAULT);
// echo ($passHash);

// var_dump(password_verify("admin", $passHash));

include_once("block/header.php");

$pdo = connectDB();

$pokemons = findAllPokemons($pdo);


?>

<div class="container">

    <h1 class="text-center"><?php echo ($title ?? "Default Title") ?></h1>

    <div class="d-flex justify-content-evenly align-items-center flex-wrap gap-3">

        <?php
        foreach ($pokemons as $pokemon) {
        ?>
            <div class="col-3 border border-primary border-2 rounded h-25">
                <img src="<?php echo ($pokemon["image"]) ?>" class="img-fluid">
                <p><?php echo ($pokemon["nameFr"]) ?></p>
                <p><?php echo ($pokemon["pokedexId"]) ?></p>
                <a href="pokemonDetail.php?id=<?php echo ($pokemon["id"]) ?>">Détail</a>
            </div>
        <?php
        }
        ?>
    </div>

</div>

<?php
include_once("block/footer.php");
?>