<?php
if (!isset($_SESSION["username"])) {
    header("Location: https://localhost/pokedex/login.php");
}

if (isset($_GET["id"]) === false) {
    header("Location: https://localhost/pokedex/admin/index.php");
}

require_once("../utils/databaseManager.php");

$pdo = connectDB();

$id = $_GET["id"];

$pokemon = findPokemonById($pdo, $id);

if ($pokemon === false) {
    header("Location: https://localhost/pokedex/admin/index.php");
}


$title = "Update";
include_once("../block/header.php");

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $pokemon = $_POST;
    $pokemon["id"] = $id;

        $errors = validateRequiredFields($_POST);
        if (empty($errors)) {
            $pdo = connectDB();
            updatePokemon($pdo, $pokemon);
            header('Location: index.php');
        }


}

?>

    <div class="container">
        <?php
        if ($_SERVER["REQUEST_METHOD"] === "POST") {

            foreach ($errors as $error) {
                echo "<p style='color: red;'>" . htmlspecialchars($error) . "</p>"; // Afficher les erreurs en rouge
            }
        }
        ?>
        <h1 class="text-center"><?php echo($title ?? "Default Title") ?></h1>
        <form action="updatePokemon.php?id=<?php echo($id) ?>" method="POST">
            <div class="form-group">
                <label for="pokedexId">ID du Pokédex :</label>
                <input type="number" class="form-control" id="pokedexId" name="pokedexId"
                       value="<?php echo($pokemon["pokedexId"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="nameFr">Nom (Français) :</label>
                <input type="text" class="form-control" id="nameFr" name="nameFr"
                       value="<?php echo($pokemon["nameFr"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="nameJp">Nom (Japonais) :</label>
                <input type="text" class="form-control" id="nameJp" name="nameJp"
                       value="<?php echo($pokemon["nameJp"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="generation">Génération :</label>
                <input type="number" class="form-control" id="generation" name="generation"
                       value="<?php echo($pokemon["generation"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="category">Catégorie :</label>
                <input type="text" class="form-control" id="category" name="category"
                       value="<?php echo($pokemon["category"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="image">URL de l'image :</label>
                <input type="text" class="form-control" id="image" name="image"
                       value="<?php echo($pokemon["pokedexId"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="imageShiny">URL de l'image (Shiny) :</label>
                <input type="text" class="form-control" id="imageShiny" name="imageShiny"
                       value="<?php echo($pokemon["imageShiny"]) ?>">
            </div>
            <div class="form-group">
                <label for="height">Hauteur :</label>
                <input type="number" class="form-control" id="height" name="height"
                       value="<?php echo($pokemon["height"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="weight">Poids :</label>
                <input type="number" class="form-control" id="weight" name="weight"
                       value="<?php echo($pokemon["weight"]) ?>" required>
            </div>
            <div class="form-group">
                <label for="catchRate">Taux de capture :</label>
                <input type="number" class="form-control" id="catchRate" name="catchRate"
                       value="<?php echo($pokemon["catchRate"]) ?>" required>
            </div>
            <input type="submit" class="btn btn-primary" value="Mettre à jour">
        </form>

    </div>
<?php
include_once("../block/footer.php");

?>