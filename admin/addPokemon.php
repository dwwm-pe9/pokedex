<?php
//session_start();
// Ce if permet de verifier la connexion, d'un utilisateur
if (!isset($_SESSION["username"])) {
    header("Location: https://localhost/pokedex/login.php");
}

require_once("../utils/databaseManager.php");
$title = "Nouveau Pokemon";

include_once("../block/header.php");

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $errors = validateRequiredFields($_POST);
    if (empty($errors)) {
        $pdo = connectDB();
        insertPokemon($pdo, $_POST);
        header('Location: index.php');
    }


}

?>

    <div class="container">

        <h1 class="text-center"><?php echo($title ?? "Default Title") ?></h1>
        <?php
        if ($_SERVER["REQUEST_METHOD"] === "POST") {

            foreach ($errors as $error) {
                echo "<p style='color: red;'>" . htmlspecialchars($error) . "</p>"; // Afficher les erreurs en rouge
            }
        }
        ?>
        <form action="addPokemon.php" method="POST">
            <div class="form-group">
                <label for="pokedexId">ID du Pokédex :</label>
                <input type="number" class="form-control" id="pokedexId" name="pokedexId" required>
            </div>
            <div class="form-group">
                <label for="nameFr">Nom (Français) :</label>
                <input type="text" class="form-control" id="nameFr" name="nameFr" required>
            </div>
            <div class="form-group">
                <label for="nameJp">Nom (Japonais) :</label>
                <input type="text" class="form-control" id="nameJp" name="nameJp" required>
            </div>
            <div class="form-group">
                <label for="generation">Génération :</label>
                <input type="number" class="form-control" id="generation" name="generation" required>
            </div>
            <div class="form-group">
                <label for="category">Catégorie :</label>
                <input type="text" class="form-control" id="category" name="category" required>
            </div>
            <div class="form-group">
                <label for="image">URL de l'image :</label>
                <input type="text" class="form-control" id="image" name="image" required>
            </div>
            <div class="form-group">
                <label for="imageShiny">URL de l'image (Shiny) :</label>
                <input type="text" class="form-control" id="imageShiny" name="imageShiny">
            </div>
            <div class="form-group">
                <label for="height">Hauteur :</label>
                <input type="number" class="form-control" id="height" name="height" required>
            </div>
            <div class="form-group">
                <label for="weight">Poids :</label>
                <input type="number" class="form-control" id="weight" name="weight" required>
            </div>
            <div class="form-group">
                <label for="catchRate">Taux de capture :</label>
                <input type="number" class="form-control" id="catchRate" name="catchRate" required>
            </div>
            <input type="submit" class="btn btn-primary" value="Ajouter Pokémon">
        </form>

    </div>

<?php
include_once("../block/footer.php");
?>