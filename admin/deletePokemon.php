<?php
if (!isset($_SESSION["username"])) {
    header("Location: https://localhost/pokedex/login.php");
}

if (isset($_GET["id"]) === false) {
    header("Location: https://localhost/pokedex/admin/index.php");
}

require_once("../utils/databaseManager.php");

$pdo = connectDB();

$id = $_GET["id"];

$pokemon = findPokemonById($pdo, $id);

if ($pokemon === false) {
    header("Location: https://localhost/pokedex/admin/index.php");
}

if ($_SERVER["REQUEST_METHOD"] === "POST")
   {
    deletePokemon($pdo,$id);
    header("Location: index.php");
   }
