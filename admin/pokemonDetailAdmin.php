<?php
if (!isset($_SESSION["username"])) {
    header("Location: https://localhost/pokedex/login.php");
}

if (isset($_GET["id"]) === false) {
    header("Location: https://localhost/pokedex/admin/index.php");
}

require_once("../utils/databaseManager.php");

$pdo = connectDB();

$id = $_GET["id"];

$pokemon = findPokemonById($pdo, $id);

if ($pokemon === false) {
    header("Location: https://localhost/pokedex/admin/index.php");
}


$title = $pokemon["nameFr"];
include_once("../block/header.php");
?>

<div class="container">

    <h1 class="text-center"><?php echo ($title ?? "Default Title") ?></h1>

    <div class="d-flex">
        <img class="" src="<?php echo ($pokemon["image"]) ?>">
        <div>
            <p>Numéro Pokedex: <?php echo ($pokemon["pokedexId"]) ?></p>
            <p>Géneration : <?php echo ($pokemon["generation"]) ?></p>
            <p>Catégorie : <?php echo ($pokemon["category"]) ?></p>
            <p>Poids: <?php echo ($pokemon["weight"]) ?></p>
            <p>Taille : <?php echo ($pokemon["height"]) ?></p>
            <p>Taux de capture : <?php echo ($pokemon["catchRate"]) ?></p>
            <a class="btn btn-primary" href="updatePokemon.php?id=<?php echo ($pokemon["id"]) ?>">Modifier</a>

            <form action="deletePokemon.php?id=<?php echo ($id) ?>" method="POST" onsubmit="return confirm('Êtes vous sur de vouloir supprimer <?php echo ($title) ?> ?');">
                <input class="btn btn-danger" type="submit" value="Supprimer">
            </form>
        </div>
    </div>
</div>


<?php
include_once("../block/footer.php");

?>