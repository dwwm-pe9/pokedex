-- MySQL dump 10.13  Distrib 5.7.33, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: pokedexpe9
-- ------------------------------------------------------
-- Server version	5.7.33

--
-- Table structure for table `pokemon`
--

DROP TABLE IF EXISTS `pokemon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pokemon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pokedexId` int(11) NOT NULL,
  `nameFr` mediumtext NOT NULL,
  `nameJp` mediumtext NOT NULL,
  `generation` int(11) NOT NULL,
  `category` mediumtext NOT NULL,
  `image` mediumtext NOT NULL,
  `imageShiny` mediumtext,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `catchRate` double DEFAULT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2053 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

Use pokedexpe9;
CREATE TABLE `utilisateur` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `username` varchar(250) NOT NULL,
 `password` varchar(250) NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `utilisateur_id_uindex` (`id`),
 UNIQUE KEY `utilisateur_username_uindex` (`username`)
)
