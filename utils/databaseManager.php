<?php

function connectDB(): PDO
{

    try {

        $host = "localhost";
        $databaseName = "pokedex";
        $user = "root";
        $password = "";

        $pdo = new PDO("mysql:host=" . $host . ";port=3306;dbname=" . $databaseName . ";charset=utf8", $user, $password);

        configPdo($pdo);

        return $pdo;
    } catch (Exception $e) {

        //Lancer l'erreur
        //throw $e;

        echo ("Erreur à la connexion: " .  $e->getMessage());

        exit();
    }
}

function configPdo(PDO $pdo): void
{
    // Recevoir les erreurs PDO ( coté SQL )
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // Choisir les indices dans les fetchs
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}

function findAllPokemons(PDO $pdo): array
{
    $reponse = $pdo->query('SELECT * FROM pokemon LIMIT 50 OFFSET 1');
    // Plusieurs resultat fetchAll
    return $reponse->fetchAll();
}


function findPokemonById(PDO $pdo, int $id): array
{
    $query = $pdo->prepare('SELECT * FROM pokemon WHERE id = :id');
    $query->execute([
        ":id" => $id
    ]);
    // Un seul resultat attendu fetch
    return $query->fetch();
}

function findPokemonByName(PDO $pdo, string $name): array
{
    $query = $pdo->prepare('SELECT * FROM pokemon WHERE name = :name');
    $query->execute([
        ":name" => $name
    ]);
    return $query->fetchAll();
}

function getUserByUsername(PDO $pdo,string $username){
        // Recuperer User avec les memes identifiants
        $response = $pdo->prepare("SELECT username, password FROM utilisateur WHERE username = :username");
        $response->execute([
            ":username" => $username
        ]);

        return $response->fetch();
}


function insertPokemon(PDO $pdo,array $pokemon){
    

// Préparer la requête
$response = $pdo->prepare("INSERT INTO pokemon (pokedexId, nameFr, nameJp, generation, category, image, imageShiny, height, weight, catchRate) 
VALUES (:pokedexId, :nameFr, :nameJp, :generation, :category, :image, :imageShiny, :height, :weight, :catchRate)");

// Exécution de la requête avec le tableau de paramètres
$response->execute([
    ':pokedexId' => $pokemon["pokedexId"],
    ':nameFr' => $pokemon["nameFr"],
    ':nameJp' => $pokemon["nameJp"],
    ':generation' => $pokemon["generation"],
    ':category' => $pokemon["category"],
    ':image' => $pokemon["image"],
    ':imageShiny' => $pokemon["imageShiny"],
    ':height' => $pokemon["height"],
    ':weight' => $pokemon["weight"],
    ':catchRate' => $pokemon["catchRate"]
    ]
);

    return $response->fetch();
}

function updatePokemon(PDO $pdo,array $pokemon){
    

    // Préparer la requête
    $response = $pdo->prepare("UPDATE pokemon SET(pokedexId = :pokedexId, nameFr = :nameFr, nameJp = :nameJp, generation = :generation, category = :category, image = :image, imageShiny, height = :height, weight = :weight, catchRate = :catchRate) WHERE id = :id");
    
    // Exécution de la requête avec le tableau de paramètres
    $response->execute([
        ':pokedexId' => $pokemon["pokedexId"],
        ':nameFr' => $pokemon["nameFr"],
        ':nameJp' => $pokemon["nameJp"],
        ':generation' => $pokemon["generation"],
        ':category' => $pokemon["category"],
        ':image' => $pokemon["image"],
        ':imageShiny' => $pokemon["imageShiny"],
        ':height' => $pokemon["height"],
        ':weight' => $pokemon["weight"],
        ':catchRate' => $pokemon["catchRate"],
        ':id' => $pokemon["id"]
        ]
    );
    
        return $response->fetch();
    }

function deletePokemon(PDO $pdo,int $id){

    $query = $pdo->prepare("DELETE FROM pokemon WHERE id = :id");

    $query->execute([
        ':id' => $id
    ]);

    return $query->fetch();

}

function validateRequiredFields(array $data) {
    $errors = [];

    // Validation de l'identifiant du Pokedex
    if (empty(trim($data['pokedexId'] ?? ''))) {
        $errors[] = "Le champ 'pokedexId' est manquant.";
    } elseif (!is_numeric(trim($data['pokedexId']))) {
        $errors[] = "Le champ 'pokedexId' doit être un entier.";
    }

    // Validation du nom français
    if (empty(trim($data['nameFr'] ?? ''))) {
        $errors[] = "Le champ 'nameFr' est manquant.";
    }

    // Validation du nom japonais
    if (empty(trim($data['nameJp'] ?? ''))) {
        $errors[] = "Le champ 'nameJp' est manquant.";
    }

    // Validation de la génération
    if (empty(trim($data['generation'] ?? ''))) {
        $errors[] = "Le champ 'generation' est manquant.";
    } elseif (!is_numeric(trim($data['generation']))) {
        $errors[] = "Le champ 'generation' doit être un entier.";
    }

    // Validation de la catégorie
    if (empty(trim($data['category'] ?? ''))) {
        $errors[] = "Le champ 'category' est manquant.";
    }

    // Validation de l'image
    if (empty(trim($data['image'] ?? ''))) {
        $errors[] = "Le champ 'image' est manquant.";
    }

    // Validation de la taille
    if (empty(trim($data['height'] ?? ''))) {
        $errors[] = "Le champ 'height' est manquant.";
    } elseif (!is_numeric(trim($data['height']))) {
        $errors[] = "Le champ 'height' doit être un nombre.";
    }

    // Validation du poids
    if (empty(trim($data['weight'] ?? ''))) {
        $errors[] = "Le champ 'weight' est manquant.";
    } elseif (!is_numeric(trim($data['weight']))) {
        $errors[] = "Le champ 'weight' doit être un nombre.";
    }

    // Validation du taux de capture
    if (empty(trim($data['catchRate'] ?? ''))) {
        $errors[] = "Le champ 'catchRate' est manquant.";
    } elseif (!is_numeric(trim($data['catchRate']))) {
        $errors[] = "Le champ 'catchRate' doit être un nombre.";
    }

    return $errors; // Retourne les erreurs trouvées
}