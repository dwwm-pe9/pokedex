<?php

if (!$_GET["fillData"]) {
    header("Location: https://localhost/pokedexApi/index.php");
}
require_once("databaseManager.php");

// URL de l'API Tyradex
$url = 'https://tyradex.tech/api/v1/pokemon';

// Récupérer les données depuis l'API
$data = file_get_contents($url);

// Convertir les données JSON en tableau associatif
$pokemonData = json_decode($data, true);


try {
    $pdo = connectDB();
    $stmt = $pdo->prepare("INSERT INTO Pokemon (pokedexId, generation, category, nameFr, nameJp, image, imageShiny, height, weight, catchRate)
                            VALUES (:pokedexId, :generationNumber, :category, :nameFr, :nameJp, :image, :imageShiny, :height, :weight, :catchRate)");

    foreach ($pokemonData as $pokemon) {
        $params = [
            ':generationNumber' => $pokemon['generation'],
            ':pokedexId' => $pokemon['pokedex_id'],
            ':category' => $pokemon['category'],
            ':nameFr' => $pokemon['name']['fr'],
            ':nameJp' => $pokemon['name']['jp'],
            ':image' => $pokemon['sprites']['regular'],
            ':imageShiny' => $pokemon['sprites']['shiny'],
            ':height' => (float)$pokemon['height'],
            ':weight' => (float)$pokemon['weight'],
            ':catchRate' => $pokemon['catch_rate']
        ];

        $stmt->execute($params);
    }

    echo "Données insérées avec succès dans la base de données.";
} catch (PDOException $e) {
    echo "Erreur : " . $e->getMessage();
}
